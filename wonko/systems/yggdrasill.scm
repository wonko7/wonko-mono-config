(define-module (wonko systems yggdrasill)
  #:use-module (gnu)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services desktop)
  #:use-module (gnu services xorg)
  #:use-module (gnu services sddm)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu services guix)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services shells)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  ;; my stuff
  #:use-module (wonko defs)
  #:use-module (wonko crew)
  #:use-module (wonko fleet)
  #:use-module (wonko dotfiles)
  #:use-module (wonko homes)
  #:use-module (wonko systems)
  #:use-module (wonko services xorg)
  #:use-module (wonko services kmonad)
  #:export (%yggdrasill-os))

(use-package-modules xorg kde synergy)

(define machine-home-services
  (list
   (simple-service
    'config-files
    home-files-service-type
    `((".x-config"
       ,(program-file
         "x-config"
         #~(system
            (string-append
             #$setxkbmap "/bin/setxkbmap -option compose:ralt us;"
             #$xrandr "/bin/xrandr --dpi 288;"
             #$xinput "/bin/xinput"
             " set-prop 'DELL07E6:00 06CB:76AF Touchpad' 'libinput Click Method Enabled' 0 1;"
             #$xinput "/bin/xinput"
             " set-prop 'DELL07E6:00 06CB:76AF Touchpad' 'libinput Accel Speed' 1.0"))))))))

(define %wonko-home
  (home-environment
    (inherit %highdpi-wonko-home)
    (services
     (cons*
      (simple-service
       'yggdrasill-shepherd home-shepherd-service-type
       (list
        (shepherd-service
         (provision '(synergy))
         (start #~(make-forkexec-constructor
                   (list #$(file-append synergy "/bin/synergy"))
                   #:log-file #$(home-log-path "synergy")))
         (stop #~(make-kill-destructor))
         (documentation "can't be arsed to move IRL"))
        (shepherd-service
         (provision '(kdeconnectd))
         (start #~(make-forkexec-constructor
                   (list #$(file-append kdeconnect "/bin/kdeconnectd"))
                   #:log-file #$(home-log-path "kdeconnectd")))
         (stop #~(make-kill-destructor))
         (documentation "ET phone home"))))
      (append
       machine-home-services
       %highdpi-wonko-services)))
    (packages (cons*
               kdeconnect
               (home-environment-packages %highdpi-wonko-home)))))

(define %media-station-home
  (home-environment
    (inherit %media-station-wonko-home)
    (services
     (append
      machine-home-services
      %media-station-wonko-services))))

(define %yggdrasill-os
  (operating-system
    (inherit %removable-laptop-os) ;; internal drive but EFI discovery is wonky
    (host-name "yggdrasill")
    (keyboard-layout %us-kb)
    (services (cons* (service slim-service-type wonko-slim-config)
                     (service noautostart-slim-service-type media-station-slim-config)
                     (service guix-home-service-type
                              `((,(crew-name %wonko) ,%wonko-home)
                                (,(crew-name %media) ,%media-station-home)))
                     (service kmonad-service-type kmonad-laptop-config)
                     (service kmonad-service-type kmonad-ergodox-config)
                     (service kmonad-service-type kmonad-bullshit-config)
                     %laptop-services))
    (mapped-devices
     (list (mapped-device
            (source (uuid "077c1391-b290-4921-ae90-f8e3cec68113"))
            (target "vault")
            (type luks-device-mapping))))
    (file-systems (let ((btrfs-vault-subvol (lambda (args)
                                              (make-vault-subvolume args mapped-devices))))
                    (cons*
                     (file-system
                       (mount-point "/boot")
                       (device (uuid "77DE-0AE2"
                                     'fat32))
                       (type "vfat"))
                     (file-system
                       (mount-point "/mnt/vault")
                       (device "/dev/mapper/vault")
                       (type "btrfs")
                       (dependencies mapped-devices))
                     (append
                      (make-vault-subvolumes mapped-devices)
                      %base-file-systems))))))

%wonko-home
%yggdrasill-os

;;; evil.el -*- lexical-binding: t; -*-
;;
;;
;;; Commentary:
;;
;;  the evil stuff
;;
;;; Code:

(use-package undo-fu)
(use-package vundo)

;; 😈
(use-package evil
  :demand t
  :after undo-fu

  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-minibuffer t)
  (setq evil-want-keybinding nil) ;; evil tells you to
  (setq evil-want-C-i-jump t)

  :config
  (evil-mode 1)
  (setq evil-undo-system 'undo-fu)
  (evil-set-undo-system evil-undo-system) ;; FIXME: this shouldn't be needed)
  ;; GREP: this concerns multi/compose key/accents/exwm-xim/input methods
  ;; (setq evil-input-method "latin-9-prefix")
  ;; I set these after evil for collection.
  ;; (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  ;; (setq evil-want-keybinding t)
  ;; (setq evil-want-minibuffer t)
  (setq evil-search-wrap nil)

  ;; fix G -> goto last line
  (evil-define-motion evil-goto-line (count)
    "Go to line COUNT. By default the last line."
    :jump t
    :type line
    (evil-ensure-column
      (if (null count)
          (goto-char (- (point-max) 1))
        (goto-char (point-min))
        (forward-line (1- count))))))

(use-package evil-collection
  :demand t
  :init
  (setq evil-want-keybinding nil)
  (setq evil-collection-calendar-want-org-bindings t)
  (setq evil-collection-setup-minibuffer t)
  (setq evil-collection-outline-bind-tab-p t)
  (setq evil-collection-key-blacklist '("SPC" "C-SPC"))
  :config
  (evil-collection-init)
  (setq evil-want-keybinding t)
  ;; (setq evil-want-C-i-jump nil)
  )


(use-package evil-escape
  :demand t
  :config
  (evil-escape)
  (evil-escape-mode 1)
  (setq evil-escape-delay 0.3
        evil-escape-key-sequence "jj"
        evil-escape-excluded-states '(normal visual multiedit emacs motion)
        ;; evil-cross-lines t
        ))

(use-package evil-matchit
  :demand t
  :config
  (global-evil-matchit-mode 1))

(use-package evil-surround
  :demand t
  :config
  (global-evil-surround-mode))

(use-package evil-exchange
  :demand t
  :config
  (setq evil-exchange-key (kbd "zx"))
  (evil-exchange-install))

(use-package evil-org
  :demand t
  ;; the equivalent for org-mode-map is in org-conf
  ;; this needs to be set after starting evil-org
  :hook (org-mode-hook
         . (lambda ()
             (evil-org-mode)
             (evil-define-key 'normal 'evil-org-mode
               (kbd "<C-return>")  '+org/insert-item-below
               (kbd "<C-S-return>") '+org/insert-item-above)))
  :config
  (setq evil-org-key-theme '(navigation insert textobjects additional shift todo heading calendar))
  (setq evil-org-retain-visual-state-on-shift t)
  (setq evil-org-special-o/O nil))

(use-package evil-org-agenda
  :demand t
  :config
  (evil-org-agenda-set-keys))

(use-package evil-snipe
  :demand t
  :hook (magit-mode-hook . turn-off-evil-snipe-override-mode)
  :config
  (setq evil-snipe-scope 'whole-visible)
  (setq evil-snipe-char-fold t)
  (setq evil-snipe-smart-case t)
  (setq evil-snipe-override-mode t)
  (evil-snipe-mode 1))

(use-package evil-leader
  :demand t
  :config
  (global-evil-leader-mode)
  (evil-leader/set-leader "<SPC>"))

(use-package evil-goggles
  :demand t
  :custom
  (evil-goggles-duration 0.500)
  (evil-goggles-pulse nil)
  :config
  (evil-goggles-mode)
  ;; fancy
  (set-face-attribute 'evil-goggles-default-face nil :foreground "white")
  (set-face-attribute 'evil-goggles-default-face nil :background "#EB64B9"))

(use-package evil-visualstar
  :demand t
  :config
  (global-evil-visualstar-mode t))

(use-package evil-commentary
  :demand t
  :config
  (evil-commentary-mode t))

;; <zoo
(use-package evil-lion
  :demand t
  :config
  (evil-lion-mode))

(use-package evil-owl
  :demand t
  :config
  (evil-owl-mode))
;; zoo>

(use-package evil-mc
  :demand t
  :init
  (setq evil-mc-cursors-map (make-sparse-keymap)) ;; FIXME: workaround on zonked req evil-mc
  :config
  (global-evil-mc-mode))

(provide 'conf/evil)
;;; evil.el ends here

#[includeIf "gitdir:/work/"]
#  path = ~/.config/git/gitconfig-work
#[includeIf "gitdir:/code/"]
#  path = ~/.config/git/gitconfig-fun

[user]
  name        = William
  email       = william@underage.wang
  signingkey  = william@underage.wang

# enable gpg by default:
## FIXME:
[commit]
  gpgsign = true
[tag]
  gpgSign = true
# see push.gpgSign

[color]
  ui = auto

# test
[column]
  ui = auto
[branch]
  sort = -committerdate

[core]
  attributesfile = ~/.config/git/attributes

[alias]
  st     = status -s
  c      = commit
  ci     = commit -a
  ca     = commit -a
  cm     = commit -m
  cim    = commit -a -m
  cam    = commit -a -m
  aip    = add -ip
  b      = branch
  ba     = branch -a
  # git b -a --column --sort=committerdate

  co     = checkout
  cob    = checkout -b
  # log, blame, also checkout: -L 12,20 path/file
  # git log -S regex
  bl     = blame -w -C -C -C
  d      = diff
  dw     = diff --word-diff -w
  dr     = diff --raw
  ds     = diff --stat
  l      = log --graph --decorate=short
  lp     = log --graph -p --decorate=short
  lw     = log --graph -p --word-diff -w --decorate=short
  ls     = log --graph --stat --decorate=short
  lr     = log --graph --raw --decorate=short
  lo     = log --graph --pretty=oneline --decorate=short
  f      = fetch
  ph     = push
  psh    = push
  pl     = pull
  pll    = pull
  r      = remote
  rs     = remote show
  ra     = remote add
  s      = submodule
  ss     = submodule summary
  si     = submodule update -j 10 --init --recursive
  su     = submodule update --recursive
  suco   = submodule update --checkout
  surb   = submodule update --rebase
  sum    = submodule update --merge
  staash = stash --all
  lsf    = ls-files
  m      = merge
  cl     = clean -fdx
  t      = tag
  ui     = update-index

# vim merge!
[merge]
  tool   = vimdiff
[mergetool]
  prompt = true
[mergetool "vimdiff"]
  cmd    = nvim -d $BASE $LOCAL $REMOTE $MERGED -c '$wincmd w' -c 'wincmd J'

# submodule stuff:
[status]
  submodulesummary = 1
[pager]
  submodule = false
  branch    = false
[push]
  recurseSubmodules = on-demand
  followTags        = true

[fetch]
  recurseSubmodules = on-demand

# misc:
[rerere]
  enabled = 1
[diff]
  renamelimit = 5000
[grep]
  patternType = perl
[pull] # merge:
  rebase = false

[diff "common-lisp"]
  xfuncname="^\\((def\\S+\\s+\\S+)"

[diff "elisp"]
  xfuncname="^\\((((def\\S+)|use-package)\\s+\\S+)"

[diff "org"]
  xfuncname="^\\*+[\t ]+(.*)$"
